<p>Olá {{ $user->getName() }},</p>

<p>Nós encontramos {{ count($jobOpportunities) > 0 ? "algumas vagas que podem " : "uma vaga que pode "  }} ser de seu interesse.</p>

@foreach ($jobOpportunities as $jobOpportunity)
    <p>
        Vaga: <a href="{{ $jobOpportunity['link'] }}">
                {{ $jobOpportunity['description'] }}
              </a>,
        pois você teve interesse em "{{ $jobOpportunity['regex'] }}"
    </p>
@endforeach

<p>Nós te desejamos boa sorte e muito sucesso!</p>

<p>PS: Esse email foi gerado por um robô, não o responda!</p>