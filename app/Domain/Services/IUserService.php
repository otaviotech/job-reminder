<?php
/**
 * Created by PhpStorm.
 * User: otavio
 * Date: 10/27/17
 * Time: 12:40 AM
 */

namespace App\Domain\Services;


interface IUserService
{
    public function get($id);
    public function getAll();
    public function save($user);
    public function create($user);
}