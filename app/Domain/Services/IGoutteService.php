<?php

namespace App\Domain\Services;

interface IGoutteService
{
    public function getInstance();
    public function get($uri,
                        array $parameters   = array(),
                        array $files        = array(),
                        array $server       = array(),
                        $content            = null,
                        $changeHistory      = true);
}