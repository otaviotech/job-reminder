<?php

namespace App\Domain\Services;

interface IGmailService
{

    public function sendEmail(array $to, $subject, $message, $cc = null);

}