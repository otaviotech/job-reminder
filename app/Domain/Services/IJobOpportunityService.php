<?php
/**
 * Created by PhpStorm.
 * User: otavio
 * Date: 10/24/17
 * Time: 12:43 AM
 */

namespace App\Domain\Services;


interface IJobOpportunityService
{
    public function getTodayOpportunities();
    public function getLastOpportunityId();
//    public function getNewestOpportunities();
//    public function getOpportunities();
}