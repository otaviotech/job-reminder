<?php

namespace App\Domain\Repositories;

interface IJobOpportunityRepository
{
    public function getLast();
    public function save($jobOpportunity);
}