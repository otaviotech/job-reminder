<?php

namespace App\Domain\Repositories;

interface IUserRepository
{
    public function get($id);
    public function getAll();
    public function save($user);
    public function create($user);
}