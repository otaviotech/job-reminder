<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobOpportunityFound extends Mailable
{
    use Queueable, SerializesModels;

    protected $jobOpportunities;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @param array $jobOpportunities
     * @param $user
     */
    public function __construct(array $jobOpportunities, $user)
    {
        $this->jobOpportunities = $jobOpportunities;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply.jobfinder@gmail.com', 'JobFinder')
            ->subject('Nova vaga encontrada, candidate-se!')
            ->view('mail.jobOpportunityFound')
            ->with([
                'jobOpportunities'  => $this->jobOpportunities,
                'user'              => $this->user
            ]);
    }
}
