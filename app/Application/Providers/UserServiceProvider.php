<?php

namespace App\Application\Providers;

use App\Domain\Services\IUserService;
use App\Infraestructure\Services\UserService;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap UserRepositoryServiceProvider to the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(IUserService::class, UserService::class);
    }
}