<?php
/**
 * Created by PhpStorm.
 * User: otavio
 * Date: 10/25/17
 * Time: 12:11 AM
 */

namespace App\Application\Providers;

use App\Domain\Repositories\IJobOpportunityRepository;
use App\Infraestructure\Repositories\JobOpportunityRepository;
use Illuminate\Support\ServiceProvider;

class JobOpportunityRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap JobOpportunityRepositoryServiceProvider to the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(IJobOpportunityRepository::class, JobOpportunityRepository::class);
    }
}