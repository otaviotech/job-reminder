<?php

namespace App\Application\Providers;

use App\Domain\Services\IGmailService;
use App\Infraestructure\Services\GmailService;
use Illuminate\Support\ServiceProvider;

class GmailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap GoutteService to the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(IGmailService::class, GmailService::class);
    }
}