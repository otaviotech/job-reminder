<?php
/**
 * Created by PhpStorm.
 * User: otavio
 * Date: 10/27/17
 * Time: 12:38 AM
 */

namespace App\Application\Providers;

use App\Domain\Repositories\IUserRepository;
use App\Infraestructure\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class UserRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap UserRepositoryServiceProvider to the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(IUserRepository::class, UserRepository::class);
    }
}