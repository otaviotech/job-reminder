<?php
/**
 * Created by PhpStorm.
 * User: otavio
 * Date: 10/24/17
 * Time: 1:14 AM
 */

namespace App\Application\Providers;


use App\Domain\Services\IGoutteService;
use App\Infraestructure\Services\GoutteService;
use Illuminate\Support\ServiceProvider;

class GoutteServiceProvider extends ServiceProvider
{
    /**
    * Bootstrap GoutteService to the application.
    *
    * @return void
    */
    public function boot()
    {
        $this->app->singleton(IGoutteService::class,GoutteService::class);
    }

}