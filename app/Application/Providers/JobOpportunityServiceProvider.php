<?php

namespace App\Application\Providers;

use App\Domain\Services\IJobOpportunityService;
use App\Infraestructure\Services\JobOpportunityService;
use Illuminate\Support\ServiceProvider;

class JobOpportunityServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap JobOpportunityServiceProvider to the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(IJobOpportunityService::class, JobOpportunityService::class);
    }
}