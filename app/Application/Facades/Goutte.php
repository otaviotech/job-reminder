<?php

namespace App\Application\Facades;

use App\Domain\Services\IGoutteService;
use Illuminate\Support\Facades\Facade;

class Goutte extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return IGoutteService::class; }
}
