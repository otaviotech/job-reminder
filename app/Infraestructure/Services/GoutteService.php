<?php

namespace App\Infraestructure\Services;

use App\Domain\Services\IGoutteService;
use Goutte\Client;

class GoutteService implements IGoutteService
{
    private $client;

    /**
     * GoutteService constructor.
     */
    public function __construct()
    {
        $this->setClient(new Client());
    }

    /**
     * @return \Goutte\Client
     */
    public function getInstance()
    {
        if ( !isset($this->client) )
        {
            $this->__construct();
        }
        return $this->getClient();
    }

    /**
     * @return \Goutte\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param \Goutte\Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    public function get($uri, array $parameters = array(), array $files = array(), array $server = array(), $content = null, $changeHistory = true)
    {
        return $this->getClient()->request('GET', $uri, $parameters, $files, $server, $content, $changeHistory);
    }
}