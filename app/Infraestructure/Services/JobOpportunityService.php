<?php

namespace App\Infraestructure\Services;

use App\Application\Facades\Goutte;
use App\Domain\Entities\JobOpportunity;
use App\Domain\Repositories\IJobOpportunityRepository;
use App\Domain\Services\IGmailService;
use App\Domain\Services\IJobOpportunityService;
use App\Domain\Services\IUserService;
use App\Mail\JobOpportunityFound;
use DateTime;
use Illuminate\Support\Facades\Mail;

class JobOpportunityService implements IJobOpportunityService
{
    protected $lastOpportunity;
    protected $jobOpportunityRepository;
    protected $userService;

    public function __construct(
        IJobOpportunityRepository $jobOpportunityRepository,
        IUserService $userService
        )
    {
        $this->jobOpportunityRepository = $jobOpportunityRepository;
        $this->userService = $userService;
    }

    public function getTodayOpportunities()
    {
        $counter = 0;

//        return $this->getLastOpportunityId();

        $opportunitiesRaw =
            Goutte::get('https://bauruempregos.com.br/home/vagas')
                ->filter('#content-left > div');

        $op = array();
        $last = 0;
        $date = "";

        for ($i = 0; $i < $opportunitiesRaw->count(); $i++)
        {
            $isDate = preg_match('/\d\d\/\d\d\/\d\d\d\d/', $opportunitiesRaw->eq($i)->html());
            if($isDate == 1){
                $counter++;
                continue;
            } else {
                if ($counter == 1)
                {
                    $jobTitle = $opportunitiesRaw->eq($i)->filter('a');
                    if($jobTitle->count() == 1){

                        $identifier = str_replace('/home/detalhes/', '', $jobTitle->attr('href'));

                        if(intval($identifier) > intval($this->getLastOpportunityId()) )
                        {
                            // Salva no banco qual foi a ultima vaga verificada.
                            if($i == 1){
                                $last = $identifier;
                            }

                            $jobTitle = preg_replace("/\r\n\t\t\t\t\t?/", '', $jobTitle->html());
                            array_push($op, [$identifier, $jobTitle]);
                        } else break;
                    }
                } else break;
            }
        }

        $last > 0 ? $this->setLastOpportunityId($last) : null;

        return $this->notifyIfAny($op);
//        return $op;
    }

    public function getLastOpportunityId()
    {
        return $this->jobOpportunityRepository
            ->getLast()
            ->getIdentifier();
    }

    private function setLastOpportunityId($id){
        $jobOpportunity = new JobOpportunity($id);
        $this->jobOpportunityRepository->save($jobOpportunity);
    }

    private function notifyIfAny(array $newOpportunities){

        if (count($newOpportunities) == 0) return false;

        $matches = array();

        $users = $this->userService->getAll();

        for($i = 0; $i < count($users); $i++)
        {
            $userInterestsString = $this->removeAccents($users[$i]->getInterests());
            $userInterests = explode(",", $userInterestsString);

            for($j = 0; $j < count($newOpportunities); $j++)
            {
                for ($k = 0; $k < count($userInterests); $k++)
                {
                    $regex = "/".$userInterests[$k]."/i";
                    $match = preg_match($regex, $newOpportunities[$j][1]);

                    if($match == 1)
                    {
                        $opportunity =
                        [
                            "regex" => $userInterests[$k],
                            "description" => $newOpportunities[$j][1],
                            "link"  => "http://bauruempregos.com.br/home/detalhes/".$newOpportunities[$j][0]
                        ];
                        if (!isset($matches[$i]) || !in_array($matches[$i], $opportunity))
                        {
                            $matches[$i]['user'] = $users[$i];
                            $matches[$i]['opportunities'][] = $opportunity;
                        }

                    }
                }
            }
        }

//        return $matches[0];
        return $this->notify($matches);
    }

    /**
     * @param $string
     * @return string
     */
    private function removeAccents($string)
    {
        $string = strtolower($string);
        $string = str_replace("á","a", $string);
        $string = str_replace("à","a", $string);
        $string = str_replace("ã","a", $string);
        $string = str_replace("â","a", $string);
        $string = str_replace("é","e", $string);
        $string = str_replace("ê","e", $string);
        $string = str_replace("í","i", $string);
        $string = str_replace("ó","o", $string);
        $string = str_replace("õ","o", $string);
        $string = str_replace("ú","u", $string);

        return $string;
    }

    private function notify(array $matches){
        for ($i = 0; $i < count($matches); $i++)
        {
            Mail::to($matches[$i]['user']->getEmail())
                ->send(new JobOpportunityFound($matches[$i]['opportunities'], $matches[$i]['user']));
        }

        return true;
    }
}
