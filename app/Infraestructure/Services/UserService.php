<?php

namespace App\Infraestructure\Services;

use App\Domain\Repositories\IUserRepository;
use App\Domain\Services\IUserService;
use Doctrine\Common\Collections\ArrayCollection;

class UserService implements IUserService
{
    protected $repository;

    public function __construct(IUserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     * @return mixed|bool|\App\Domain\Entities\User
     */
    public function get($id)
    {
        if($id > 0){
            return $this->repository->get($id);
        } else return false;
    }


    /**
     * @return ArrayCollection|\App\Domain\Entities\User
     */
    public function getAll()
    {
        return $this->repository->getAll();
    }

    /**
     * @param \App\Domain\Entities\User $user
     */
    public function save($user)
    {
        // validar...
        return $this->repository->save($user);
    }

    /**
     * @param \App\Domain\Entities\User $user
     */
    public function create($user)
    {
        // validar...
        return $this->repository->create($user);
    }
}