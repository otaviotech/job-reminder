<?php

namespace App\Infraestructure\Services;

use App\Domain\Services\IGmailService;
use Google_Client;
use Google_Service_Gmail;

class GmailService implements IGmailService
{
    private $client;
    private $service;

    /**
     * GmailService constructor.
     */
    public function __construct()
    {
        $this->setClient(new Google_Client());
        $this->setService(new Google_Service_Gmail($this->getClient()));
    }

    /**
     * @return Google_Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Google_Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
        $this->client->setClientId(env('GMAIL_CLIENT_ID'));
        $this->client->setClientSecret(env('GMAIL_CLIENT_SECRET'));
        $this->client->setApplicationName(env('APP_NAME'));
        $this->client->setDeveloperKey(env('GMAIL_API_KEY'));
        $this->client->setScopes(array(Google_Service_Gmail::MAIL_GOOGLE_COM));
    }

    /**
     * @return Google_Service_Gmail
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param Google_Service_Gmail $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    public function getInstance()
    {
        if ( !isset($this->client) )
        {
            $this->__construct();
        }
        return $this->getClient();
    }

    public function sendEmail(array $to, $subject, $message, $cc = null)
    {
//       $m= new \Google_Service_Gmail_Message();
        return $this->getService()->users_messages->listUsersMessages('me', ['maxResults' => 2, "labelIds" => 'INBOX']);

    }

}