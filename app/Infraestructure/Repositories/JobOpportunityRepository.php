<?php

namespace App\Infraestructure\Repositories;

use App\Domain\Entities\JobOpportunity;
use App\Domain\Repositories\IJobOpportunityRepository;
use LaravelDoctrine\ORM\Facades\EntityManager;

class JobOpportunityRepository implements IJobOpportunityRepository
{

    /**
     * @return JobOpportunity
     */
    public function getLast()
    {
        return EntityManager::find('App\Domain\Entities\JobOpportunity', 1);
    }

    /**
     * @param JobOpportunity $jobOpportunity
     * @return mixed
     */
    public function save($jobOpportunity){
        $jobOpportunity->setId(1);
        EntityManager::merge($jobOpportunity);
        EntityManager::flush();
        return $jobOpportunity;
    }
}