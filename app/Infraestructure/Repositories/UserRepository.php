<?php

namespace App\Infraestructure\Repositories;

use App\Domain\Entities\User;
use App\Domain\Repositories\IUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use LaravelDoctrine\ORM\Facades\EntityManager;

class UserRepository implements IUserRepository
{

    /**
     * @param integer $id
     * @return User
     */
    public function get($id)
    {
        return EntityManager::find('App\Domain\Entities\User', $id);
    }

    /**
     * @return ArrayCollection|User
     */
    public function getAll()
    {
        return EntityManager::getRepository(User::class)->findAll();
    }

    /**
     * @param User $user
     */
    public function save($user)
    {
        EntityManager::merge($user);
        EntityManager::flush();
    }

    /**
     * @param User $user
     */
    public function create($user)
    {
        EntityManager::persist($user);
        EntityManager::flush();
    }


}