<?php

use App\Domain\Entities\JobOpportunity;
use App\Domain\Entities\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Cria o holder para saber se a vaga ja foi verificada.
        $jobOpportunity = new JobOpportunity(1);

        $user1 = new User(
            'Otávio Araújo',
            'otaviotech@gmail.com',
            'ti,tecnologia,inforcacao,suporte,help,service,desk,tecnico,programador,desenvolvedor,developer,dev,web,c#,asp,.net,unix,javascript,java script,js,angular,vue,vuejs'
        );

        $user2 = new User(
            'Giovanna Ferreira',
            'giovanna.adm15@gmail.com',
            'contabilidade,contabeis,controladoria,adm,administracao,tributario,tributaria'
        );

        EntityManager::persist($jobOpportunity);
        EntityManager::persist($user1);
        EntityManager::persist($user2);
        EntityManager::flush();



    }
}
